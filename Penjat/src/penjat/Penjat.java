package penjat;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.ArrayList;
import java.util.Scanner;


public class Penjat {
	public static void main(String[] args) {
	    
		int maxIntents = 10;
		int intentsIncorrectes = 0;
		String paraula = paraulaRandom(); //funcio per a asignar una paraula aleatoria
		
		
		ArrayList<Character> lletresDescobertes = new ArrayList<>(); //creem una llista per a posr les paraules descobertes
		
		System.out.println("Endevina la paraula:");

		boolean paraulaEncertada = false;
		
		while (!paraulaEncertada && intentsIncorrectes <= maxIntents)
		{
                    StringBuilder paraulaX = updateParaulaX(paraula,lletresDescobertes);
                    mostraMissatge(intentsIncorrectes, paraula, lletresDescobertes, paraulaX);
			
                    System.out.println("\nEscriu una lletra: ");
		    Scanner reader = new Scanner (System.in); //creem objecte scanner
                    char lletra = reader.next(".").charAt(0); //demanem lletra
		     
		     int resultat = comprovaLletra(lletra, paraula, lletresDescobertes);
		     
		     switch(resultat)
		     {
		     	case 1: System.out.println("La lletra esta repetida!"); break;
		     	case 2: System.out.println("Correcte la lletra existeix!"); break;
		     	case 3: { System.out.println("La lletra no existeix!"); intentsIncorrectes++; } break;
		     	default : System.out.print("error");
		     }
                     
		     pintaDibuix(intentsIncorrectes);
                     
		     if(lletresDescobertes.size() == paraula.length())
		     {
		    	 System.out.println("Has endivinat tota la paraula!");
                         paraulaEncertada = true;
		     }
		}
                
                if(paraulaEncertada)         
                {
                    System.out.println("HAS GUANYAT");
                }
                else{
                     System.out.println("S'han acabat tots els intents");
                     System.out.println("HAS PERDUT");
                }

	}
	
	public static String paraulaRandom()
	{
		Random rand = new Random();
		int randomNum = rand.nextInt(10);
		
		String paraules[] = {"cotxe", "rodo", "ratoli", "tigre", "submari", "teclat", "sabata", "fletxa", "liquid", "llum"};
		return paraules[randomNum];            
	}
	
	public static StringBuilder updateParaulaX(String paraula, ArrayList<Character> lletresDescobertes)
	{
		StringBuilder p = new StringBuilder("           ");
		for(int x = 0; x< paraula.length(); x++)
		{
			if(lletresDescobertes.contains(paraula.charAt(x)))
			{
                            char word = paraula.charAt(x);    
                            p.setCharAt(x, word);
			}
			else 
			{
				p.setCharAt(x, '*');
			}
		}
		return p;
		
	}
	
	public static void mostraMissatge(int intentsIncorrectes, String paraula, ArrayList<Character> lletresDescobertes, StringBuilder paraulaX)
	{       if(intentsIncorrectes == 10) {
                    System.out.println("Portes " + intentsIncorrectes + " intents incorrectes. Aquest sera el teu ultim intent.");
                } else {
                    System.out.println("Portes " + intentsIncorrectes + " intents incorrectes.");
                }
		System.out.println("Has endevinat "  +lletresDescobertes.size() + " lletres de " + paraula.length() + " lletres que te la paraula.");
		System.out.println("Paraula: " + paraulaX);
		
	}
	
	public static int comprovaLletra(char lletra, String paraula, ArrayList<Character> lletresDescobertes)
	{	
            boolean repetides = false;
            boolean existeix = false;
            int vegades = 0;
            
		for(int x=0; x < paraula.length(); x++) 
		{
			if (paraula.charAt(x) == lletra) //Si existeix la lletra
			{
                            vegades++;
                            existeix = true;
			}
		}
                
                if (existeix) {
                    if(lletresDescobertes.isEmpty())
                    {
                        lletresDescobertes.add(lletra);
                        return 2;
                    } else {

                        for (int y=0; y < lletresDescobertes.size(); y++) 
                        {
                            if (lletresDescobertes.get(y) == lletra) //Si esta repetida
                            {
                                repetides = true; 
                            }
                        }

                        if(repetides)
                        {
                            return 1;
                        } else {
                            for(int i = 0; i < vegades; i++)
                            {
                                lletresDescobertes.add(lletra);
                            }
                            
                            return 2;
                        }
                    }
                } else {
                    
                    return 3;
                }
		
		//return 0;
	}
        
        public static void pintaDibuix(int intentsIncorrectes) {
            if(intentsIncorrectes == 0){
                System.out.println(
                                   "\n" +
                                   "\n" +
                                   "\n" +
                                   "\n" +
                                   "");
            } else if (intentsIncorrectes == 1) {
                System.out.println(
                                   "\n" +
                                   "\n" +
                                   "\n" +
                                   "\n" +
                                   " _____");
            }else if (intentsIncorrectes == 2) {
                System.out.println(
                                   "\n" +
                                   "|\n" +
                                   "|\n" +
                                   "|\n" +
                                   "|_____");
            }else if (intentsIncorrectes == 3) {
                System.out.println(
                                   "____\n" +
                                   "|  \n" +
                                   "| \n" +
                                   "| \n" +
                                   "|_____");
            }else if (intentsIncorrectes == 4) {
                System.out.println(
                                   "____\n" +
                                   "|  o\n" +
                                   "| \n" +
                                   "| \n" +
                                   "|_____");
            }else if (intentsIncorrectes == 5) {
                System.out.println(
                                   "____\n" +
                                   "|  o\n" +
                                   "| /\n" +
                                   "| \n" +
                                   "|_____");
            }else if (intentsIncorrectes == 6) {
                System.out.println(
                                   "____\n" +
                                   "|  o\n" +
                                   "| /O\n" +
                                   "| \n" +
                                   "|_____");
            }else if (intentsIncorrectes == 7) {
                System.out.println(
                                   "____\n" +
                                   "|  o\n" +
                                   "| /O\\\n" +
                                   "| \n" +
                                   "|_____");
            }else if (intentsIncorrectes == 8) {
                System.out.println(
                                   "____\n" +
                                   "|  o\n" +
                                   "| /O\\\n" +
                                   "| /\n" +
                                   "|_____");
            }else if (intentsIncorrectes == 9) {
                System.out.println(
                                   "____\n" +
                                   "|  o\n" +
                                   "| /O\\\n" +
                                   "| / \\\n" +
                                   "|_____");
            } else if(intentsIncorrectes == 10){
                System.out.println(
                                   "____\n" +
                                   "|  o\n" +
                                   "| /O\\\n" +
                                   "| /.\\\n" +
                                   "|_____");
            }
        }

}
